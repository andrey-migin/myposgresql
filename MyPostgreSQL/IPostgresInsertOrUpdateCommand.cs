using System;

namespace MyPostgreSQL
{
    public interface IPostgresDapperInsertOrUpdate : IPostgresDapperBase
    {
        IPostgresDapperInsertOrUpdate SetInsertModel(object insertModel, string primaryKeyName);
        IPostgresDapperInsertOrUpdate SetUpdateModel(object updateModel);

    }

    public class PostgresInsertOrUpdateCommand : PostgresDapperBase, IPostgresDapperInsertOrUpdate
    {

        private readonly string _tableName;

        private string _insertString;
        private string _updateString;

        private string _pkName;

        public PostgresInsertOrUpdateCommand(IPostgresConnection connectionString, string tableName) : base(
            connectionString)
        {
            _tableName = connectionString.GenerateTableName(tableName);
        }

        protected override string GenerateBeforeWhere()
        {
            if (_insertString == null)
                throw new Exception("Please initialize insert model");

            return
                $"INSERT INTO {_tableName} {_insertString} ON CONFLICT ON CONSTRAINT {_pkName} DO UPDATE SET {_updateString}";
        }

        public IPostgresDapperInsertOrUpdate SetInsertModel(object updateModel, string primaryKeyName)
        {
            _pkName = primaryKeyName;

            var (fields, _) = SqlOperation.Insert.GenerateFieldsString(updateModel);
            var values = SqlOperation.Insert.GenerateValues(updateModel);

            _insertString = $"({fields}) VALUES ({values})";

            return this;
        }

        public IPostgresDapperInsertOrUpdate SetUpdateModel(object updateModel)
        {
            var (fields, count) = SqlOperation.Update.GenerateFieldsString(updateModel);
            var values = SqlOperation.Update.GenerateValues(updateModel);

            _updateString = count == 1 
                ? fields+"="+values 
                : "(" + fields + ")=(" + values + ")";

            return this;
        }

    }

    public static class PostgresInsertOrUpdateCommandHelpers
    {
        public static IPostgresDapperInsertOrUpdate InsertOrUpdate(this IPostgresConnection connectionString,
            string tableName)
        {
            return new PostgresInsertOrUpdateCommand(connectionString, tableName);
        }
    }
}