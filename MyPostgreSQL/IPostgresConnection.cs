namespace MyPostgreSQL
{
    public interface IPostgresConnection
    {
        string ConnectionString { get; }
        string Scheme { get; }
    }

    public class PostgresConnection : IPostgresConnection
    {

        public PostgresConnection(string connectionString, string scheme = null)
        {
            ConnectionString = connectionString;
            Scheme = scheme;
        }
        public string ConnectionString { get; }

        public string Scheme { get; }

    }
}